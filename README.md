# go-config

An [Archaius](https://github.com/Netflix/archaius)-like configuration manager.

## Example

```go
m1 := sources.NewMemorySource()
m2 := sources.NewConsulSource("http://my-consul-address:8500")
c := NewConfigurator(m1, m2)

di := c.GetInt("xyz", 1)
ds := c.GetString("abc", "default")

di.Value() // 1
ds.Value() // default

m.Set("abc", "custom")
// Change value in consul :: xyz to 42

ds.Value() // custom
di.Value() // 42
```

## Integrations
* In-memory
* Consul (TODO)
