package config

import (
	"bitbucket.org/to-increase/go-config/dynamic"
	"bitbucket.org/to-increase/go-config/sources"
	"bitbucket.org/to-increase/go-config/types"
	log "bitbucket.org/to-increase/go-logger"
	"github.com/pkg/errors"
	"sync"
)

type Configuration interface {
	// Create a derivative of this configuration object in a certain namespace.
	// e.g. namespaced 'foo' will query foo/bar when asked for 'bar'.
	Namespace(namespace string) Configuration
	// Retrieve an int value from the key/value store. If the value is not found, the provided default will be used.
	GetInt(key string, def int) types.DynamicIntValue
	// Retrieve an int value from the k/v store or an error if it can't be found.
	GetMandatoryInt(key string) (types.DynamicIntValue, error)
	// Retrieve a string value from the key/value store
	GetString(key string, def string) types.DynamicStringValue
	// Retrieve a string value from the k/v store or an error if it can't be found.
	GetMandatoryString(key string) (types.DynamicStringValue, error)
	// Retrieve a boolean value from the key/value store
	GetBool(key string, def bool) types.DynamicBoolValue
	// Retrieve a bool value from the k/v store or an error if it can't be found.
	GetMandatoryBool(key string) (types.DynamicBoolValue, error)
	// Register a callback when a value changes within the current scope.
	OnChange(types.Callback)
	// List the subscriptions to dynamic values that this key store currently has:
	Subscriptions() []string
}

type cv struct {
	sources   []sources.ConfigurationSource
	store     *store
	callbacks []types.Callback
	logger    *log.Logger
	mutex     sync.RWMutex
}

func NewConfiguratorWithLogger(l *log.Logger, sources ...sources.ConfigurationSource) Configuration {
	cv := &cv{
		sources:   sources,
		store:     newStore(),
		callbacks: []types.Callback{},
		logger:    l.WithField("go-config", true),
	}

	for _, v := range sources {
		v.SetLogger(cv.logger.WithField("go-config-source", v.Name()))
		go updater(cv, v.Changes())
	}

	return cv
}

func NewConfigurator(sources ...sources.ConfigurationSource) Configuration {
	return NewConfiguratorWithLogger(log.NewScopedLogger(), sources...)
}

func (c *cv) getActualValue(p types.Path) (bool, interface{}) {
	pp := p.GetPath()
	for _, s := range c.sources {
		found, val := s.Find(pp)
		if found {
			c.logger.Debugf("GetActualValue for path %v found: %v", pp, val)
			return true, val
		}
	}
	c.logger.Debugf("GetActualValue for path %v NOT found.", pp)
	return false, nil
}

func (c *cv) Namespace(namespace string) Configuration {
	return newNamespacedConfig(namespace, c)
}

func (c *cv) OnChange(cb types.Callback) {
	c.mutex.Lock()
	defer c.mutex.Unlock()
	c.callbacks = append(c.callbacks, cb)
}

func updater(cv *cv, change <-chan sources.ChangeSet) {
	for {
		evt := <-change
		p := changesetToPath(evt)

		go handleCallbacks(cv, p, evt)

		cv.mutex.RLock()
		if cv.store.Has(p) {
			dyn := cv.store.Get(p)
			dynamic.UpdateValue(dyn, p, evt.Value)
		}
		cv.mutex.RUnlock()
	}
}

func handleCallbacks(cv *cv, p types.Path, evt sources.ChangeSet) {
	cv.mutex.RLock()
	cbs := cv.callbacks
	cv.mutex.RUnlock()

	for _, cb := range cbs {
		cb(p, evt.Value)
	}
}

func (c *cv) getFromCache(path types.Path) interface{} {
	// A previous value exists
	if c.store.Has(path) {
		c.logger.Debugf("Get[...]() with key %v; cached entry found.", path)
		return c.store.Get(path)
	}
	return nil
}

// And the type-specific getters are a bit nasty, except for types they are mostly identical.

func (c *cv) GetInt(key string, def int) types.DynamicIntValue {
	c.mutex.Lock()
	defer c.mutex.Unlock()
	p := keyToPath(key)
	// A previous value exists
	cached := c.getFromCache(p)
	if cached != nil {
		dyn, ok := cached.(types.DynamicIntValue)
		if ok {
			return dyn
		}
		c.logger.Error("Type error! Stored dynamic value is not an int but something else.")
	}

	// Does an actual value exist in any of the sources?
	found, f := c.getActualValue(p)
	var d *dynamic.DynIntVal

	if found {
		c.logger.Debugf("GetInt() with key '%v'; new entry with val '%v', def '%v'.", key, f, def)
		d, e := dynamic.NewDynInt(f, def)
		if e == nil {
			c.store.Add(p, d)
			return d
		}
		c.logger.Errorf("Error -- '%v' is not an int. Returning defaults", f)
	}
	c.logger.Debugf("GetInt() with key '%v'; new with default entry '%v'.", key, def)
	d, _ = dynamic.NewDynInt(def, def)

	c.store.Add(p, d)
	return d
}

func (c *cv) GetMandatoryInt(key string) (types.DynamicIntValue, error) {
	p := keyToPath(key)
	cached := c.getFromCache(p)
	if cached != nil {
		dyn, ok := cached.(types.DynamicIntValue)
		if ok {
			return dyn, nil
		} else {
			return nil, errors.New("Can't cast cached value to an int.")
		}

	}
	found, _ := c.getActualValue(p)
	if found {
		// default value is ignored, we already know it exists
		return c.GetInt(key, 0), nil
	} else {
		return nil, errors.Errorf("Can't find an int on path %v", p)
	}
}

func (c *cv) GetString(key string, def string) types.DynamicStringValue {
	c.mutex.Lock()
	defer c.mutex.Unlock()
	p := keyToPath(key)
	// A previous value exists
	cached := c.getFromCache(p)
	if cached != nil {
		dyn, ok := cached.(types.DynamicStringValue)
		if ok {
			return dyn
		}
		c.logger.Errorf("Actually: %v\n", c.store.Get(p).(types.DynamicIntValue))
		c.logger.Errorf("Type error! Stored dynamic value is not a string but something else.")
	}

	// Does an actual value exist in any of the sources?
	found, f := c.getActualValue(p)
	var d *dynamic.DynStringVal

	if found {
		c.logger.Debugf("GetString() with key '%v'; new entry with val %v, def '%v'.", key, f, def)
		d = dynamic.NewDynString(f.(string), def)
	} else {
		c.logger.Debugf("GetString() with key '%v'; new default entry '%v'.", key, def)
		d = dynamic.NewDynString(def, def)
	}

	c.store.Add(p, d)
	return d
}

func (c *cv) GetMandatoryString(key string) (types.DynamicStringValue, error) {
	p := keyToPath(key)
	cached := c.getFromCache(p)
	if cached != nil {
		dyn, ok := cached.(types.DynamicStringValue)
		if ok {
			return dyn, nil
		} else {
			return nil, errors.New("Can't cast cached value to a string.")
		}

	}
	found, _ := c.getActualValue(p)
	if found {
		// default value is ignored, we already know it exists
		return c.GetString(key, ""), nil
	} else {
		return nil, errors.Errorf("Can't find a string on path %v", p)
	}
}

func (c *cv) GetBool(key string, def bool) types.DynamicBoolValue {
	c.mutex.Lock()
	defer c.mutex.Unlock()
	p := keyToPath(key)
	// A previous value exists
	cached := c.getFromCache(p)
	if cached != nil {
		dyn, ok := cached.(types.DynamicBoolValue)
		if ok {
			return dyn
		}
		c.logger.Error("Type error! Stored dynamic value is not a string but something else.")
	}

	// Does an actual value exist in any of the sources?
	found, f := c.getActualValue(p)
	var d *dynamic.DynBoolVal

	if found {
		c.logger.Debugf("GetBool() with key %v; new entry with val %v, def %v.", key, f, def)
		d, e := dynamic.NewDynBool(f, def)
		if e == nil {
			c.store.Add(p, d)
			return d
		}
		c.logger.Errorf("Error -- '%v' is not an int. Returning defaults", f)
	} else {
		c.logger.Debugf("GetBool() with key %v; new default entry %v.", key, def)
		d, _ = dynamic.NewDynBool(def, def)
	}

	c.store.Add(p, d)
	return d
}

func (c *cv) GetMandatoryBool(key string) (types.DynamicBoolValue, error) {
	p := keyToPath(key)
	cached := c.getFromCache(p)
	if cached != nil {
		dyn, ok := cached.(types.DynamicBoolValue)
		if ok {
			return dyn, nil
		} else {
			return nil, errors.New("Can't cast cached value to a string.")
		}

	}
	found, _ := c.getActualValue(p)
	if found {
		// default value is ignored, we already know it exists
		return c.GetBool(key, false), nil
	} else {
		return nil, errors.Errorf("Can't find a bool on path %v", p)
	}
}

func (c *cv) Subscriptions() []string {
	ret := make([]string, 0, 0)
	for k := range c.store.data {
		ret = append(ret, k)
	}
	return ret
}
