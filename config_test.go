package config

import (
	"bitbucket.org/to-increase/go-config/sources"
	"bitbucket.org/to-increase/go-config/types"
	"bitbucket.org/to-increase/go-logger"
	"fmt"
	"github.com/stretchr/testify/assert"
	"os"
	"testing"
	"time"
)

func TestWithoutExistingConsul(t *testing.T) {
	s, err := sources.NewConsulSource("an-address")
	assert.Nil(t, err)
	c := NewConfigurator(s)
	d := c.GetString("foo", "default")
	assert.Equal(t, "default", d.Value())
}

func TestCreateWithCustomLogger(t *testing.T) {
	s, err := sources.NewConsulSource("an-address")
	assert.Nil(t, err)
	c := NewConfiguratorWithLogger(logger.NewScopedLogger(), s)
	assert.NotNil(t, c)
}

func TestGetExistingValue(t *testing.T) {
	m := sources.NewMemorySource()
	c := NewConfigurator(m)
	m.Set("abc", "custom")

	d := c.GetString("abc", "default")

	assert.Equal(t, "custom", d.Value())
}

func TestRetrieveMandatoryInt(t *testing.T) {
	m := sources.NewMemorySource()
	c := NewConfigurator(m)
	m.Set("abc", 42)

	d, e := c.GetMandatoryInt("abc")

	assert.Nil(t, e)
	assert.Equal(t, 42, d.Value())
}

func TestRetrieveMissingMandatoryInt(t *testing.T) {
	c := NewConfigurator()

	d, e := c.GetMandatoryInt("abc")

	assert.NotNil(t, e)
	assert.Nil(t, d)
}

func TestRetrieveMandatoryString(t *testing.T) {
	m := sources.NewMemorySource()
	c := NewConfigurator(m)
	m.Set("x/y/z", "aaa")

	d, e := c.GetMandatoryString("x/y/z")

	assert.Nil(t, e)
	assert.Equal(t, "aaa", d.Value())
}

func TestRetrieveMissingMandatoryString(t *testing.T) {
	c := NewConfigurator()

	d, e := c.GetMandatoryInt("x/y/z")

	assert.NotNil(t, e)
	assert.Nil(t, d)
}

func TestRetrieveMandatoryBool(t *testing.T) {
	m := sources.NewMemorySource()
	c := NewConfigurator(m)
	m.Set(".", false)

	d, e := c.GetMandatoryBool(".")

	assert.Nil(t, e)
	assert.Equal(t, false, d.Value())
}

func TestRetrieveMissingMandatoryBool(t *testing.T) {
	c := NewConfigurator()

	d, e := c.GetMandatoryInt(".")

	assert.NotNil(t, e)
	assert.Nil(t, d)
}

func TestDynamicString(t *testing.T) {
	m := sources.NewMemorySource()
	c := NewConfigurator(m)

	d := c.GetString("abc", "default")

	assert.Equal(t, "default", d.Value())

	m.Set("abc", "custom")

	time.Sleep(50 * time.Millisecond)

	assert.Equal(t, "custom", d.Value())
}

func TestDynamicBool(t *testing.T) {
	m := sources.NewMemorySource()
	c := NewConfigurator(m)

	d := c.GetBool("abc", true)

	assert.Equal(t, true, d.Value())

	m.Set("abc", false)

	time.Sleep(50 * time.Millisecond)

	assert.Equal(t, false, d.Value())
}

func TestDynamicIntRemoval(t *testing.T) {
	m := sources.NewMemorySource()
	c := NewConfigurator(m)
	m.Set("abc", 123)

	d := c.GetInt("abc", 0)

	time.Sleep(50 * time.Millisecond)

	assert.Equal(t, 123, d.Value())
	m.Remove("abc")

	time.Sleep(50 * time.Millisecond)

	assert.Equal(t, 0, d.Value())
}

func TestDynamicStringIncludingCallback(t *testing.T) {
	m := sources.NewMemorySource()
	c := NewConfigurator(m)
	hit := false

	c.OnChange(func(p types.Path, v interface{}) {
		assert.Equal(t, "custom", v)
		hit = true
	})

	d := c.GetString("abc", "default")

	assert.Equal(t, "default", d.Value())

	m.Set("abc", "custom")

	time.Sleep(50 * time.Millisecond)

	assert.Equal(t, "custom", d.Value())
	assert.True(t, hit)
}

func TestDynamicInt(t *testing.T) {
	m := sources.NewMemorySource()
	c := NewConfigurator(m)

	d := c.GetInt("abc", 123)

	assert.Equal(t, 123, d.Value())

	m.Set("abc", 1234)

	time.Sleep(50 * time.Millisecond)

	assert.Equal(t, 1234, d.Value())
}

func TestHierarchicalDynamicStrings(t *testing.T) {
	m := sources.NewMemorySource()
	c := NewConfigurator(m)

	d1 := c.GetString("abc/def/ghi", "default")
	d2 := c.GetString("abc/defg", "default2")

	assert.Equal(t, "default", d1.Value())
	assert.Equal(t, "default2", d2.Value())

	m.Set("abc/def/ghi", "custom")

	time.Sleep(50 * time.Millisecond)

	assert.Equal(t, "custom", d1.Value())
	assert.Equal(t, "default2", d2.Value())
}

func TestReadIntegerDynVal(t *testing.T) {
	src := sources.NewPropertiesSource(fmt.Sprintf("sources%cproptest.properties", os.PathSeparator))
	c := NewConfigurator(src)

	d1 := c.GetInt("integer", 3)

	time.Sleep(50 * time.Millisecond)

	assert.Equal(t, 5, d1.Value())
}

func TestReadFromMultipleSources(t *testing.T) {
	propsrc := sources.NewPropertiesSource(fmt.Sprintf("sources%cproptest.properties", os.PathSeparator))
	consulsrc, err := sources.NewConsulSource("localhost:8500")
	assert.Nil(t, err)
	c := NewConfigurator(propsrc, consulsrc)
	v := c.GetString("foo", "defaultfoo")

	time.Sleep(50 * time.Millisecond)

	assert.Equal(t, "fooval", v.Value())
}

func TestShouldNotPanicOnChangingTypes(t *testing.T) {
	done := make(chan struct{})

	go func() {
		c := NewConfigurator()

		d1 := c.GetInt("foo", 3)
		d2 := c.GetString("foo", "foo")

		assert.Equal(t, 3, d1.Value())
		assert.Equal(t, "foo", d2.Value())
		close(done)
	}()

	select {
	case <-done:
	case <-time.After(1 * time.Second):
		assert.Fail(t, "Timeout: test did not finish in a reasonable timeframe.")
	}
}

func TestSubscriptions(t *testing.T) {
	s, err := sources.NewConsulSource("an-address")
	assert.Nil(t, err)
	c := NewConfigurator(s)
	assert.NotNil(t, c)

	c.GetString("/abc/def", "z")
	c.GetInt("/ghi/jkl", 11)
	c.GetBool("mno/pqr", true) // Leading slashes are stripped internally

	subs := c.Subscriptions()

	assert.Len(t, subs, 3)
	assert.Contains(t, subs, "abc/def")
	assert.Contains(t, subs, "ghi/jkl")
	assert.Contains(t, subs, "mno/pqr")
}
