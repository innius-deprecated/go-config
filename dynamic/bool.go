package dynamic

import (
	"bitbucket.org/to-increase/go-config/types"
	"errors"
	"fmt"
	"strconv"
	"sync"
)

type DynBoolVal struct {
	value     bool
	initial   bool
	update    chan BoolUpdate
	callbacks []types.BoolCallback
	mutex     sync.RWMutex
	defaultCallback types.Callback
}

type BoolUpdate struct {
	Value interface{}
	Path  types.Path
}

func NewDynBool(actual interface{}, initial interface{}) (*DynBoolVal, error) {
	source := make(chan BoolUpdate, 1)

	act, err := as_bool(actual)
	if err != nil {
		return nil, err
	}

	init, err := as_bool(initial)
	if err != nil {
		return nil, err
	}

	v := &DynBoolVal{
		value:     act,
		initial:   init,
		update:    source,
		callbacks: []types.BoolCallback{},
	}

	go func() {
		for {
			update := <-source
			v.mutex.Lock()
			if update.Value == nil {
				// The value is deleted, revert back to the default value
				v.value = v.initial
			} else {
				// We have a new value.
				val, err := as_bool(update.Value)
				if err != nil {
					continue
				}
				v.value = val
			}
			v.mutex.Unlock()
			// But only a read lock to communicate it.
			v.mutex.RLock()
			if len(v.callbacks) == 0 && v.defaultCallback != nil {
				v.defaultCallback(update.Path, v.value)
			} else {
				for _, cb := range v.callbacks {
					cb(update.Path, v.value)
				}
			}
			v.mutex.RUnlock()
		}
	}()

	return v, nil
}

func (v *DynBoolVal) Update(u BoolUpdate) {
	go func() {
		v.update <- u
	}()
}

func (v *DynBoolVal) Value() bool {
	v.mutex.RLock()
	defer v.mutex.RUnlock()
	return v.value
}

func (v *DynBoolVal) OnChange(cb types.BoolCallback) {
	v.mutex.Lock()
	defer v.mutex.Unlock()
	v.callbacks = append(v.callbacks, cb)
}

func (v *DynBoolVal) String() string {
	return fmt.Sprintf("DynamicBoolValue: %v (initial: %v)", v.value, v.initial)
}

func as_bool(v interface{}) (bool, error) {
	switch v := v.(type) {
	case bool:
		return v, nil
	case string:
		return strconv.ParseBool(v)
	default:
		return false, errors.New("Invalid actual type")
	}
}

func (v *DynBoolVal) SetDefaultOnChange(cb types.Callback) {
	v.defaultCallback = cb
}
