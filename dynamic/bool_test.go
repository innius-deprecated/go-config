package dynamic_test

import (
	"bitbucket.org/to-increase/go-config/dynamic"
	"bitbucket.org/to-increase/go-config/types"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func newBool(val bool) dynamic.BoolUpdate {
	return dynamic.BoolUpdate{
		Path:  newPath("bool", "foo", "bar"),
		Value: val,
	}
}

func TestBoolPropertyChange(t *testing.T) {
	d, e := dynamic.NewDynBool(true, true)
	assert.Nil(t, e)
	assert.Equal(t, true, d.Value())

	d.Update(newBool(false))

	// Allow a bit of time to propagate the change.
	time.Sleep(25 * time.Millisecond)

	assert.Equal(t, false, d.Value())
}

func TestBoolPropertyChangeFromString(t *testing.T) {
	d, e := dynamic.NewDynBool(true, true)
	assert.Nil(t, e)
	assert.Equal(t, true, d.Value())

	d.Update(dynamic.BoolUpdate{
		Path:  newPath("bool", "foo"),
		Value: "false",
	})

	// Allow a bit of time to propagate the change.
	time.Sleep(25 * time.Millisecond)

	assert.Equal(t, false, d.Value())
}

func TestBoolCallback(t *testing.T) {
	d, e := dynamic.NewDynBool(false, false)
	assert.Nil(t, e)
	hit := false

	d.OnChange(func(path types.Path, value bool) {
		assert.Equal(t, true, value)
		assert.Equal(t, "foo/bar/bool", path.GetPath())
		hit = true
	})

	d.Update(newBool(true))

	// Allow a bit of time to propagate the change.
	time.Sleep(25 * time.Millisecond)
	assert.Equal(t, true, hit)
}
