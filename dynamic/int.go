package dynamic

import (
	"bitbucket.org/to-increase/go-config/types"
	"errors"
	"fmt"
	"strconv"
	"sync"
)

type DynIntVal struct {
	value     int
	initial   int
	update    chan IntUpdate
	callbacks []types.IntCallback
	mutex     sync.RWMutex
	defaultCallback types.Callback
}

type IntUpdate struct {
	Value interface{}
	Path  types.Path
}

func NewDynInt(actual interface{}, initial interface{}) (*DynIntVal, error) {
	source := make(chan IntUpdate, 1)

	act, err := as_int(actual)
	if err != nil {
		return nil, err
	}

	init, err := as_int(initial)
	if err != nil {
		return nil, err
	}

	v := &DynIntVal{
		value:     act,
		initial:   init,
		update:    source,
		callbacks: []types.IntCallback{},
	}

	go func() {
		for {
			update := <-source
			// We need a write lock to change the value
			v.mutex.Lock()
			if update.Value == nil {
				// The value is deleted, revert back to the default value
				v.value = v.initial
			} else {
				// We have a new value.
				val, err := as_int(update.Value)
				if err != nil {
					continue
				}
				v.value = val
			}
			v.mutex.Unlock()
			// But only a read lock to communicate it.
			v.mutex.RLock()
			if len(v.callbacks) == 0 && v.defaultCallback != nil {
				v.defaultCallback(update.Path, v.value)
			} else {
				for _, cb := range v.callbacks {
					cb(update.Path, v.value)
				}
			}
			v.mutex.RUnlock()
		}
	}()

	return v, nil
}

func (v *DynIntVal) Update(u IntUpdate) {
	go func() {
		v.update <- u
	}()
}

func (v *DynIntVal) Value() int {
	v.mutex.RLock()
	defer v.mutex.RUnlock()
	return v.value
}

func (v *DynIntVal) OnChange(cb types.IntCallback) {
	v.mutex.Lock()
	defer v.mutex.Unlock()
	v.callbacks = append(v.callbacks, cb)
}

func (v *DynIntVal) String() string {
	return fmt.Sprintf("DynamicIntValue: %v (initial: %v)", v.value, v.initial)
}

func as_int(v interface{}) (int, error) {
	switch v := v.(type) {
	case int:
		return v, nil
	case string:
		return strconv.Atoi(v)
	default:
		return 0, errors.New("Invalid actual type")
	}
}

func (v *DynIntVal) SetDefaultOnChange(cb types.Callback) {
	v.defaultCallback = cb
}
