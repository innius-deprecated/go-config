package dynamic_test

import (
	"bitbucket.org/to-increase/go-config/dynamic"
	"bitbucket.org/to-increase/go-config/types"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func newInt(val interface{}) dynamic.IntUpdate {
	return dynamic.IntUpdate{
		Path:  newPath("int", "foo", "bar"),
		Value: val,
	}
}

func TestIntPropertyChange(t *testing.T) {
	d, e := dynamic.NewDynInt(-1, -1)
	assert.Nil(t, e)
	assert.Equal(t, -1, d.Value())

	d.Update(newInt(42))

	// Allow a bit of time to propagate the change.
	time.Sleep(25 * time.Millisecond)

	assert.Equal(t, 42, d.Value())
}

func TestIntCallback(t *testing.T) {
	d, e := dynamic.NewDynInt(-1, -1)
	assert.Nil(t, e)
	hit := false

	d.OnChange(func(path types.Path, value int) {
		assert.Equal(t, 123, value)
		assert.Equal(t, "foo/bar/int", path.GetPath())
		hit = true
	})

	d.Update(newInt(123))

	// Allow a bit of time to propagate the change.
	time.Sleep(25 * time.Millisecond)
	assert.Equal(t, true, hit)
}

func TestIntPropertyChangeFromString(t *testing.T) {
	d, e := dynamic.NewDynInt(-1, -1)
	assert.Nil(t, e)
	assert.Equal(t, -1, d.Value())

	d.Update(newInt("123"))

	// Allow a bit of time to propagate the change.
	time.Sleep(25 * time.Millisecond)

	assert.Equal(t, 123, d.Value())
}

func TestNewDynIntFromInterface(t *testing.T) {
	var a interface{} = "123"
	var b interface{} = "123"
	d, e := dynamic.NewDynInt(a, b)
	assert.Nil(t, e)
	assert.Equal(t, 123, d.Value())
}

func TestNewDynIntFromInvalidInterface(t *testing.T) {
	var a interface{} = "123"
	var b interface{} = "abc"
	_, e := dynamic.NewDynInt(a, b)
	assert.NotNil(t, e)
}
