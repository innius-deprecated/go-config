package dynamic_test

import "bitbucket.org/to-increase/go-config/types"

func newPath(key string, prefix ...string) types.Path {
	return types.Path{
		Prefix: prefix,
		Key:    key,
	}
}
