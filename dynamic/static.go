package dynamic

import (
	"bitbucket.org/to-increase/go-config/types"
	"fmt"
)

func UpdateValue(dyn interface{}, p types.Path, value interface{}) {
	switch v := dyn.(type) {
	case *DynIntVal:
		v.Update(IntUpdate{
			Path:  p,
			Value: value,
		})
	case *DynStringVal:
		v.Update(StringUpdate{
			Path:  p,
			Value: value,
		})
	case *DynBoolVal:
		v.Update(BoolUpdate{
			Path:  p,
			Value: value,
		})
	default:
		panic(fmt.Sprintf("Unkown type: %v", v))
	}
}
