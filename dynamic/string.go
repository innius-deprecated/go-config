package dynamic

import (
	"bitbucket.org/to-increase/go-config/types"
	"fmt"
	"sync"
)

type DynStringVal struct {
	value     string
	initial   string
	update    chan StringUpdate
	callbacks []types.StringCallback
	mutex     sync.RWMutex
	defaultCallback types.Callback
}

type StringUpdate struct {
	Value interface{}
	Path  types.Path
}

func NewDynString(actual string, initial string) *DynStringVal {
	source := make(chan StringUpdate, 1)

	v := &DynStringVal{
		value:     actual,
		initial:   initial,
		update:    source,
		callbacks: []types.StringCallback{},
	}

	go func() {
		for {
			update := <-source
			v.mutex.Lock()
			if update.Value == nil {
				// The value is deleted, revert back to the default value
				v.value = v.initial
			} else {
				// We have a new value.
				v.value = update.Value.(string)
			}
			v.mutex.Unlock()
			// we only need a read lock to propagate these changes.
			v.mutex.RLock()
			if len(v.callbacks) == 0 && v.defaultCallback != nil {
				v.defaultCallback(update.Path, v.value)
			} else {
				for _, cb := range v.callbacks {
					cb(update.Path, v.value)
				}
			}
			v.mutex.RUnlock()
		}
	}()

	return v
}

func (v *DynStringVal) Update(u StringUpdate) {
	go func() {
		v.update <- u
	}()
}

func (v *DynStringVal) Value() string {
	v.mutex.RLock()
	defer v.mutex.RUnlock()
	return v.value
}

func (v *DynStringVal) OnChange(cb types.StringCallback) {
	v.mutex.Lock()
	defer v.mutex.Unlock()
	v.callbacks = append(v.callbacks, cb)
}

func (v *DynStringVal) String() string {
	return fmt.Sprintf("DynamicStringValue: %v (initial: %v)", v.value, v.initial)
}

func (v *DynStringVal) SetDefaultOnChange(cb types.Callback) {
	v.defaultCallback = cb
}