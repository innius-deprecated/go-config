package dynamic_test

import (
	"bitbucket.org/to-increase/go-config/dynamic"
	"bitbucket.org/to-increase/go-config/types"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func newString(val string) dynamic.StringUpdate {
	return dynamic.StringUpdate{
		Path:  newPath("string", "foo", "bar"),
		Value: val,
	}
}

func TestStringPropertyChange(t *testing.T) {
	d := dynamic.NewDynString("default", "default")
	assert.Equal(t, "default", d.Value())

	d.Update(newString("foo"))

	// Allow a bit of time to propagate the change.
	time.Sleep(25 * time.Millisecond)

	assert.Equal(t, "foo", d.Value())
}

func TestStringCallback(t *testing.T) {
	d := dynamic.NewDynString("default", "default")
	hit := false

	d.OnChange(func(path types.Path, value string) {
		assert.Equal(t, "coffee", value)
		assert.Equal(t, "foo/bar/string", path.GetPath())
		hit = true
	})
	
	d.Update(newString("coffee"))

	// Allow a bit of time to propagate the change.
	time.Sleep(25 * time.Millisecond)
	assert.Equal(t, true, hit)
}
