package factory

import (
	"bitbucket.org/to-increase/go-config/dynamic"
	"bitbucket.org/to-increase/go-config/types"
)

// Create a new dynamic string value with static contents.
// Handy while creating APIs that currently are not really dynamic
func DynamicStringValue(value string) types.DynamicStringValue {
	dv := dynamic.NewDynString(value, value)
	return dv
}

// Create a new dynamic int value with static contents.
// Handy while creating APIs that currently are not really dynamic
func DynamicIntValue(value int) types.DynamicIntValue {
	dv, _ := dynamic.NewDynInt(value, value)
	return dv
}

// Create a new dynamic bool value with static contents.
// Handy while creating APIs that currently are not really dynamic
func DynamicBoolValue(value bool) types.DynamicBoolValue {
	dv, _ := dynamic.NewDynBool(value, value)
	return dv
}
