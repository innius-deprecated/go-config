package factory

import (
	"testing"
	"github.com/stretchr/testify/assert"
)

func TestDynamicStringValueFactory(t *testing.T) {
	val := DynamicStringValue("foo.bar.baz")
	assert.Equal(t, "foo.bar.baz", val.Value())
}
