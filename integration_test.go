package config

import (
	"fmt"
	"net/http"
	"strings"
	"testing"
	"time"

	"bitbucket.org/to-increase/go-config/sources"
	"github.com/hashicorp/consul/api"
	"github.com/hashicorp/consul/watch"
	"github.com/stretchr/testify/assert"
)

// To run these tests, you need consul:
// docker run -it --rm --name=dev-consul -p 8500:8500 consul agent -dev -bind 0.0.0.0 -client 0.0.0.0 -ui
// curl -X PUT -d 'hello' http://localhost:8500/v1/kv/foo/string
// curl -X PUT -d '42' http://localhost:8500/v1/kv/foo/number
// curl -X PUT -d 'true' http://localhost:8500/v1/kv/foo/bool
// It can't easily be done with mb, because we also need to set consul-specific headers and these have to be unique on every request etc
var should_run = false

func TestShowDisclaimer(t *testing.T) {
	if !should_run {
		fmt.Println()
		fmt.Println("Some important tests rely on consul. They are currently DISABLED")
		fmt.Println("See the 'integration_tests.go' file for details.")
		fmt.Println()
	}
}

func set_string(t *testing.T, key string, value string) {
	// update the value
	body := strings.NewReader(value)
	req, err := http.NewRequest("PUT", fmt.Sprintf("http://localhost:8500/v1/kv%v", key), body)
	assert.Nil(t, err)
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	resp, err := http.DefaultClient.Do(req)
	assert.Nil(t, err)
	defer resp.Body.Close()
}

func TestConsulIntegration(t *testing.T) {
	if !should_run {
		return
	}
	m, err := sources.NewConsulSource("localhost:8500")
	assert.Nil(t, err)
	c := NewConfigurator(m)

	set_string(t, "/foo/string", "hello")

	d1, e := c.GetMandatoryString("/foo/string")
	assert.Nil(t, e)
	d2 := c.GetString("/foo/string", "default")

	assert.Equal(t, "hello", d1.Value())
	assert.Equal(t, "hello", d2.Value())

	set_string(t, "/foo/string", "bye")

	time.Sleep(50 * time.Millisecond)

	assert.Equal(t, "bye", d1.Value())
	assert.Equal(t, "bye", d2.Value())
}

func TestConsulGetStringProperty(t *testing.T) {
	if !should_run {
		return
	}
	set_string(t, "/foo/string", "hello")
	s, err := sources.NewConsulSource("localhost:8500")
	assert.Nil(t, err)
	found, actual := s.Find("foo/string")
	assert.True(t, found)
	assert.Equal(t, "hello", actual)
}

func TestConsulGetIntProperty(t *testing.T) {
	if !should_run {
		return
	}
	set_string(t, "/foo/number", "42")
	s, err := sources.NewConsulSource("localhost:8500")
	assert.Nil(t, err)
	found, actual := s.Find("foo/number")
	assert.True(t, found)
	assert.Equal(t, "42", actual)
}

func TestConsulGetBoolProperty(t *testing.T) {
	if !should_run {
		return
	}
	set_string(t, "/foo/bool", "true")
	s, err := sources.NewConsulSource("localhost:8500")
	assert.Nil(t, err)
	found, actual := s.Find("foo/bool")
	assert.True(t, found)
	assert.Equal(t, "true", actual)
}

func TestWatchParse(t *testing.T) {
	if !should_run {
		return
	}
	conf := map[string]interface{}{
		"key":  "foo/string",
		"type": "key",
	}
	plan, err := watch.Parse(conf)
	assert.Nil(t, err)
	assert.NotNil(t, plan)
	hit := false

	plan.Handler = func(idx uint64, val interface{}) {
		kvpair := val.(*api.KVPair)
		var result string = string(kvpair.Value)
		assert.Equal(t, "hello", result)
		hit = true
	}

	go func(wp *watch.Plan) {
		if err := wp.Run("localhost:8500"); err != nil {
			assert.Fail(t, "watchplan failed: ", err)
		}
	}(plan)

	time.Sleep(10 * time.Millisecond)
	plan.Stop()
	assert.Equal(t, true, hit)
}
