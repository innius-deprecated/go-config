package config

import (
	"bitbucket.org/to-increase/go-config/sources"
	"bitbucket.org/to-increase/go-config/types"
	"strings"
)

func changesetToPath(c sources.ChangeSet) types.Path {
	return keyToPath(c.Key)
}

func keyToPath(key string) types.Path {
	if strings.Index(key, types.Separator) == 0 {
		key = key[1:]
	}
	splitted := strings.Split(key, types.Separator)
	key, pfx := splitted[len(splitted)-1], splitted[:len(splitted)-1]
	return types.Path{
		Prefix: pfx,
		Key:    key,
	}
}
