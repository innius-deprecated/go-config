package config

import (
	"bitbucket.org/to-increase/go-config/sources"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestChangesetToPath(t *testing.T) {
	c := sources.ChangeSet{
		Key:   "foo/bar/baz",
		Value: "oops",
	}
	p := changesetToPath(c)
	assert.Equal(t, "foo", p.Prefix[0])
	assert.Equal(t, "bar", p.Prefix[1])
	assert.Equal(t, "baz", p.Key)
}

func TestMappingLeadingSeparatorShouldBeSkipped(t *testing.T) {
	c := sources.ChangeSet{
		Key:   "/foo/bar/baz",
		Value: "oops",
	}
	p := changesetToPath(c)
	assert.Equal(t, "foo", p.Prefix[0])
	assert.Equal(t, "bar", p.Prefix[1])
	assert.Equal(t, "baz", p.Key)
}
