package config

import (
	"bitbucket.org/to-increase/go-config/types"
	"fmt"
)

type namespaced_cv struct {
	prefix string
	cv     *cv
}

func newNamespacedConfig(prefix string, cv *cv) Configuration {
	return &namespaced_cv{
		prefix: prefix,
		cv:     cv,
	}
}

func (c *namespaced_cv) Namespace(namespace string) Configuration {
	return newNamespacedConfig(c.key(namespace), c.cv)
}

func (c *namespaced_cv) key(key string) string {
	return fmt.Sprintf("%s/%s", c.prefix, key)
}

func (c *namespaced_cv) OnChange(cb types.Callback) {
	c.cv.OnChange(cb)
}

func (c *namespaced_cv) GetInt(key string, def int) types.DynamicIntValue {
	return c.cv.GetInt(c.key(key), def)
}

func (c *namespaced_cv) GetString(key string, def string) types.DynamicStringValue {
	return c.cv.GetString(c.key(key), def)
}

func (c *namespaced_cv) GetBool(key string, def bool) types.DynamicBoolValue {
	return c.cv.GetBool(c.key(key), def)
}

func (c *namespaced_cv) GetMandatoryInt(key string) (types.DynamicIntValue, error) {
	return c.cv.GetMandatoryInt(c.key(key))
}

func (c *namespaced_cv) GetMandatoryString(key string) (types.DynamicStringValue, error) {
	return c.cv.GetMandatoryString(c.key(key))
}

func (c *namespaced_cv) GetMandatoryBool(key string) (types.DynamicBoolValue, error) {
	return c.cv.GetMandatoryBool(c.key(key))
}

func (c *namespaced_cv) Subscriptions() []string {
	return c.cv.Subscriptions()
}
