package config

import (
	"testing"
	"bitbucket.org/to-increase/go-config/sources"
	"github.com/stretchr/testify/assert"
)

func TestSingleNamespace(t *testing.T) {
	m := sources.NewMemorySource()
	m.Set("abc/def", "custom")

	c := NewConfigurator(m)

	ns := c.Namespace("abc")
	d := ns.GetString("def", "default")
	assert.Equal(t, "custom", d.Value())
}

func TestDoubleNamespace(t *testing.T) {
	m := sources.NewMemorySource()
	m.Set("abc/def/ghi", "custom")

	c := NewConfigurator(m)

	ns1 := c.Namespace("abc")
	ns2 := ns1.Namespace("def")

	d1 := ns1.GetString("def/ghi", "default")
	d2 := ns2.GetString("ghi", "default")

	assert.Equal(t, "custom", d1.Value())
	assert.Equal(t, "custom", d2.Value())
}
