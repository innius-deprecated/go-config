package sources

import (
	"net/http"
	"sync"
	"time"

	log "bitbucket.org/to-increase/go-logger"
	"github.com/hashicorp/consul/api"
	"github.com/hashicorp/consul/watch"
)

type ConsulConfigurationSource struct {
	address string
	changes []chan ChangeSet
	watches map[string]*watch.Plan
	config  *api.Config
	kv      *api.KV
	log     *log.Logger
	mutex   sync.RWMutex
}

func NewConsulSource(address string) (*ConsulConfigurationSource, error) {
	c := api.DefaultConfig()
	c.Address = address
	c.HttpClient = &http.Client{
		Timeout: 3 * time.Second,
	}
	client, err := api.NewClient(c)

	if err != nil {
		return nil, err
	}

	config_source := &ConsulConfigurationSource{
		address: address,
		changes: make([]chan ChangeSet, 0, 0),
		watches: make(map[string]*watch.Plan),
		config:  c,
		kv:      client.KV(),
	}

	return config_source, nil
}

func (c *ConsulConfigurationSource) Name() string {
	return "ConsulConfigurationSource"
}

func (c *ConsulConfigurationSource) register_watch(key string, current_value interface{}) {
	if _, ok := c.watches[key]; ok {
		return // there is an existing watch on this key
	}
	conf := map[string]interface{}{
		"key":  key,
		"type": "key",
	}
	plan, err := watch.Parse(conf)
	if err != nil {
		if c.log != nil {
			c.log.Error(err)
		}
		return
	}

	plan.Handler = func(idx uint64, val interface{}) {
		kvpair, ok := val.(*api.KVPair)
		if ok {
			val := string(kvpair.Value)
			if current_value == nil || current_value != val {
				current_value = val
				c.update(kvpair.Key, val)
			}
		} else {
			if c.log != nil {
				c.log.Debugf("Unknown result from watchplan handler: %+v", val)
			}
		}
	}

	go func(wp *watch.Plan) {
		if err := wp.Run(c.address); err != nil {
			if c.log != nil {
				c.log.Error(err)
			}
		}
	}(plan)

	c.watches[key] = plan
}

// Fetch a value from consul's k/v store.
func (c *ConsulConfigurationSource) fetch(key string) interface{} {
	q := &api.QueryOptions{
		AllowStale: true,
	}

	c.mutex.RLock()
	pair, _, _ := c.kv.Get(key, q)
	c.mutex.RUnlock()

	if pair == nil {
		return nil
	} else {
		return string(pair.Value)
	}
}

// Propagate an update of a value through to subscribers.
func (c *ConsulConfigurationSource) update(key string, value interface{}) {
	cs := ChangeSet{
		Key:   key,
		Value: value,
	}

	if c.log != nil {
		if value == nil {
			c.log.Warningf("Configuration key '%v' disappeared -- reverting to the default value.", key)
		} else {
			c.log.Infof("Configuration key '%v' updated to '%v'", key, value)
		}
	}

	c.mutex.RLock()
	chans := c.changes
	c.mutex.RUnlock()

	go func() {
		for _, c := range chans {
			c <- cs
		}
	}()
}

func (c *ConsulConfigurationSource) SetLogger(l *log.Logger) {
	c.log = l
}

func (c *ConsulConfigurationSource) Changes() <-chan ChangeSet {
	cs := make(chan ChangeSet)
	c.changes = append(c.changes, cs)
	return cs
}

func (c *ConsulConfigurationSource) Find(key string) (bool, interface{}) {
	// Register a watcher for it so that it gets automatically refreshed.
	val := c.fetch(key)
	c.register_watch(key, val)
	return (val != nil), val
}

func (c *ConsulConfigurationSource) Dispose() {
	c.mutex.RLock()
	chans := c.changes
	c.mutex.RUnlock()

	for _, w := range c.watches {
		w.Stop()
	}

	for _, ch := range chans {
		close(ch)
	}
}
