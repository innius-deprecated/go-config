package sources

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestConsulInterfaceCompliance(t *testing.T) {
	var s ConfigurationSource
	s, err := NewConsulSource("an-address")
	assert.Nil(t, err)
	assert.NotNil(t, s)
}
