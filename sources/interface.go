package sources

import log "bitbucket.org/to-increase/go-logger"

type ChangeSet struct {
	Key   string
	Value interface{}
}

type ConfigurationSource interface {
	// The name of this configuration source
	Name() string
	// Configure a logger in this configuration source
	SetLogger(*log.Logger)
	// Try to find a concrete value
	Find(key string) (bool, interface{})
	// Retrieve a channel to get notified of changes
	Changes() <-chan ChangeSet
	// Dispose all resources -- destroy the configuration source
	Dispose()
}
