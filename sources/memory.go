package sources

import log "bitbucket.org/to-increase/go-logger"

type MemoryConfigurationSource struct {
	values  map[string]interface{}
	changes []chan ChangeSet
	log     *log.Logger
}

func NewMemorySource() *MemoryConfigurationSource {
	return &MemoryConfigurationSource{
		values:  make(map[string]interface{}),
		changes: make([]chan ChangeSet, 0, 0),
	}
}

func (m *MemoryConfigurationSource) Name() string {
	return "MemoryConfigurationSource"
}

func (m *MemoryConfigurationSource) propagate(cs ChangeSet) {
	for _, c := range m.changes {
		c <- cs
	}
}

func (c *MemoryConfigurationSource) SetLogger(l *log.Logger) {
	c.log = l
}

func (m *MemoryConfigurationSource) Set(key string, value interface{}) {
	m.values[key] = value

	cs := ChangeSet{
		Key:   key,
		Value: value,
	}

	go m.propagate(cs)
}

func (m *MemoryConfigurationSource) Remove(key string) {
	delete(m.values, key)

	cs := ChangeSet{
		Key:   key,
		Value: nil,
	}

	go m.propagate(cs)
}

func (m *MemoryConfigurationSource) Changes() <-chan ChangeSet {
	c := make(chan ChangeSet)
	m.changes = append(m.changes, c)
	return c
}

func (m *MemoryConfigurationSource) Find(key string) (bool, interface{}) {
	value, exists := m.values[key]
	return exists, value
}

func (c *MemoryConfigurationSource) Dispose() {
	for _, ch := range c.changes {
		close(ch)
	}
}
