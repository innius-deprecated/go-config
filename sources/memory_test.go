package sources

import (
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestMemoryInterfaceCompliance(t *testing.T) {
	var s ConfigurationSource
	s = NewMemorySource()
	assert.NotNil(t, s)
}

func TestMemoryChangeChannel(t *testing.T) {
	src := NewMemorySource()

	evt := src.Changes()

	hitcnt := 0

	go func() {
		cs := <-evt
		assert.Equal(t, "hello", cs.Key)
		assert.Equal(t, "world", cs.Value)
		hitcnt++
	}()

	src.Set("hello", "world")

	time.Sleep(25 * time.Millisecond)
	assert.Equal(t, 1, hitcnt)
}

func TestMemoryMultipleChangeChannel(t *testing.T) {
	src := NewMemorySource()

	cs1 := src.Changes()
	cs2 := src.Changes()

	src.Set("hello", "world")

	cs := <-cs1
	assert.Equal(t, "hello", cs.Key)
	assert.Equal(t, "world", cs.Value)

	cs = <-cs2
	assert.Equal(t, "hello", cs.Key)
	assert.Equal(t, "world", cs.Value)
}

func TestMemoryChangeChannelMultiplePaths(t *testing.T) {
	src := NewMemorySource()

	evt := src.Changes()

	src.Set("hello", "world")
	time.Sleep(50 * time.Millisecond)
	src.Set("bye", "world")

	cs := <-evt
	assert.Equal(t, "hello", cs.Key)
	assert.Equal(t, "world", cs.Value)

	cs = <-evt
	assert.Equal(t, "bye", cs.Key)
	assert.Equal(t, "world", cs.Value)
}

func TestMemoryChangeChannelIdenticalKeys(t *testing.T) {
	src := NewMemorySource()

	evt := src.Changes()

	src.Set("bye", "world")
	src.Set("bye", "world")
	src.Set("bye", "world")

	<-evt
	<-evt
	<-evt
}
