package sources

import (
	"fmt"
	"strings"
	"sync"
	"time"

	log "bitbucket.org/to-increase/go-logger"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/ssm"
	"github.com/aws/aws-sdk-go/service/ssm/ssmiface"
	"github.com/pkg/errors"
)

// ParameterStoreConfigurationSource is a configuration source for AWS Parameter Store
type ParameterStoreConfigurationSource struct {
	cache   map[string]interface{}
	changes []chan ChangeSet
	watch   *ssmWatch
	log     *log.Logger
	mutex   sync.RWMutex
	client  ssmiface.SSMAPI
}

type ssmWatch struct {
	path  string
	props *ParameterStoreConfigurationSource
}

func (c *ParameterStoreConfigurationSource) exportVariables(path string, nextToken string, pc chan *ssm.Parameter) error {
	input := &ssm.GetParametersByPathInput{
		Path:           &path,
		WithDecryption: aws.Bool(true),
		Recursive:      aws.Bool(true),
	}

	if nextToken != "" {
		input.SetNextToken(nextToken)
	}

	output, err := c.client.GetParametersByPath(input)
	if err != nil {
		return errors.Wrapf(err, "could not retrieve parameters for path %s", path)
	}

	for _, p := range output.Parameters {
		pc <- p
	}

	if output.NextToken != nil {
		err := c.exportVariables(path, *output.NextToken, pc)
		if err != nil {
			return err
		}
	}

	return nil
}

func (fw *ssmWatch) read() map[string]interface{} {
	pc := make(chan *ssm.Parameter, 1)

	go func() {
		if err := fw.props.exportVariables(fw.path, "", pc); err != nil {
			fmt.Println(err)
		}
		close(pc)
	}()

	res := make(map[string]interface{})
	for p := range pc {
		if p != nil {
			name := *p.Name
			key := strings.Trim(name[len(fw.path):], "/")
			res[key] = *p.Value
		}
	}
	return res
}

func updateParameters(p *ParameterStoreConfigurationSource) {
	contents := p.watch.read()
	for k, v := range contents {
		p.provide(k, v)
	}
}

func ssmWatchdog(p *ParameterStoreConfigurationSource) {
	for {
		updateParameters(p)
		time.Sleep(default_interval)
	}
}

// NewParameterStoreConfigurationSource creates a default AWS parameter store configuration
func NewParameterStoreConfigurationSource(environment, serviceName string) *ParameterStoreConfigurationSource {
	session := session.Must(session.NewSession())

	return newParameterStoreConfigurationSource(fmt.Sprintf("/%s/%s", environment, serviceName), ssm.New(session))
}

func newParameterStoreConfigurationSource(path string, client ssmiface.SSMAPI) *ParameterStoreConfigurationSource {

	configSource := &ParameterStoreConfigurationSource{
		cache:   make(map[string]interface{}),
		changes: make([]chan ChangeSet, 0, 0),
		log:     log.NewScopedLogger(),
		watch: &ssmWatch{
			path: path,
		},
		client: client,
	}

	configSource.watch.props = configSource

	updateParameters(configSource)

	go ssmWatchdog(configSource)

	return configSource
}

// Name returns the name of the current parameter store
func (c *ParameterStoreConfigurationSource) Name() string {
	return "ParameterStoreConfigurationSource"
}

// provide a value, updating if changed
func (c *ParameterStoreConfigurationSource) provide(key string, value interface{}) {
	c.mutex.Lock()
	val := c.cache[key]
	if val != value {
		c.cache[key] = value
	}
	c.mutex.Unlock()
	// the update can be sent without holding the lock here.
	if val != value {
		c.update(key, value)
	}
}

// Propagate an update of a value through to subscribers.
func (c *ParameterStoreConfigurationSource) update(key string, value interface{}) {
	cs := ChangeSet{
		Key:   key,
		Value: value,
	}

	if c.log != nil {
		if value == nil {
			c.log.Warningf("Configuration key '%v' disappeared -- reverting to the default value.", key)
		} else {
			c.log.Infof("Configuration key '%v' updated to '%v'", key, value)
		}
	}

	go func() {
		c.mutex.RLock()
		chans := c.changes
		c.mutex.RUnlock()
		for _, c := range chans {
			c <- cs
		}
	}()
}

// SetLogger defines the logger for the store
func (c *ParameterStoreConfigurationSource) SetLogger(l *log.Logger) {
	c.log = l
}

// Changes returns a channel for value changes
func (c *ParameterStoreConfigurationSource) Changes() <-chan ChangeSet {
	cs := make(chan ChangeSet)
	c.mutex.Lock()
	c.changes = append(c.changes, cs)
	c.mutex.Unlock()
	return cs
}

// Find returns the value for a specified key
func (c *ParameterStoreConfigurationSource) Find(key string) (bool, interface{}) {
	// Register a watcher for it so that it gets automatically refreshed.

	c.mutex.Lock()
	defer c.mutex.Unlock()
	val, found := c.cache[key]
	return found, val
}

// Dispose closes the channels
func (c *ParameterStoreConfigurationSource) Dispose() {
	c.mutex.RLock()
	chans := c.changes
	c.mutex.RUnlock()
	for _, ch := range chans {
		close(ch)
	}
}
