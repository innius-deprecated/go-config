package sources

import (
	"testing"
	"time"

	"bitbucket.org/to-increase/go-config/sources/mocks"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/ssm"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func TestParameterStoreInterfaceCompliance(t *testing.T) {

	mockSSM := new(mocks.SSMAPI)

	mockSSM.On("GetParametersByPath", mock.Anything).Return(&ssm.GetParametersByPathOutput{}, nil)
	var s ConfigurationSource
	s = newParameterStoreConfigurationSource("/plantage7/authentication-service", mockSSM)
	assert.NotNil(t, s)

}

func TestParameterStoreFindKey(t *testing.T) {
	mockSSM := new(mocks.SSMAPI)
	output := &ssm.GetParametersByPathOutput{
		Parameters: []*ssm.Parameter{
			{Name: aws.String("/foo/bar"), Value: aws.String("bar")},
		},
		NextToken: aws.String("foo"),
	}

	i := 0
	mockSSM.
		On("GetParametersByPath", mock.Anything).
		Return(output, nil).
		Run(func(mock.Arguments) {
			if i > 0 {
				output.Parameters = []*ssm.Parameter{
					{Name: aws.String("/foo/bar/baz"), Value: aws.String("baz")},
				}
				output.NextToken = nil
			}
			i++
		})

	src := newParameterStoreConfigurationSource("/foo/bar", mockSSM)

	time.Sleep(10 * time.Millisecond)

	found, fooval := src.Find("baz")

	notfound, noneval := src.Find("abc")

	assert.True(t, found)
	assert.Equal(t, "baz", fooval)

	assert.False(t, notfound)
	assert.Nil(t, noneval)
}
