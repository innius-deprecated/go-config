package sources

import (
	log "bitbucket.org/to-increase/go-logger"
	"bufio"
	"os"
	"strings"
	"sync"
	"time"
)

type PropertiesConfigurationSource struct {
	address string
	cache   map[string]interface{}
	changes []chan ChangeSet
	watch   *file_watch
	log     *log.Logger
	mutex   sync.RWMutex
}

type file_watch struct {
	file       string
	has_logged bool
	props      *PropertiesConfigurationSource
}

func (fw *file_watch) read() map[string]interface{} {
	f, err := os.Open(fw.file)
	if err != nil {
		if !fw.has_logged {
			fw.props.log.Errorf("Cannot read properties from file %v: %v -- config file will be ignored.", fw.file, err)
			// only log this error once. We need to keep polling to reflect configs that might eventually exist.
			fw.has_logged = true
		}
		return nil
	}
	defer f.Close()

	res := make(map[string]interface{})

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		if strings.HasPrefix(line, "#") {
			continue
		}
		splitted := strings.Split(line, "=")
		if len(splitted) == 2 {
			res[splitted[0]] = splitted[1]
		}
	}
	return res
}

func update(p *PropertiesConfigurationSource) {
	contents := p.watch.read()
	for k, v := range contents {
		p.provide(k, v)
	}
}

func properties_watchdog(p *PropertiesConfigurationSource) {
	for {
		update(p)
		time.Sleep(default_interval)
	}
}

func NewPropertiesSource(file string) *PropertiesConfigurationSource {
	config_source := &PropertiesConfigurationSource{
		cache:   make(map[string]interface{}),
		changes: make([]chan ChangeSet, 0, 0),
		log:     log.NewScopedLogger(),
		watch: &file_watch{
			file: file,
		},
	}

	config_source.watch.props = config_source

	update(config_source)

	go properties_watchdog(config_source)

	return config_source
}

func (p *PropertiesConfigurationSource) Name() string {
	return "PropertiesConfigurationSource"
}

// provide a value, updating if changed
func (c *PropertiesConfigurationSource) provide(key string, value interface{}) {
	c.mutex.Lock()
	val := c.cache[key]
	if val != value {
		c.cache[key] = value
	}
	c.mutex.Unlock()
	// the update can be sent without holding the lock here.
	if val != value {
		c.update(key, value)
	}
}

// Propagate an update of a value through to subscribers.
func (c *PropertiesConfigurationSource) update(key string, value interface{}) {
	cs := ChangeSet{
		Key:   key,
		Value: value,
	}

	if c.log != nil {
		if value == nil {
			c.log.Warningf("Configuration key '%v' disappeared -- reverting to the default value.", key)
		} else {
			c.log.Infof("Configuration key '%v' updated to '%v'", key, value)
		}
	}

	go func() {
		c.mutex.RLock()
		chans := c.changes
		c.mutex.RUnlock()
		for _, c := range chans {
			c <- cs
		}
	}()
}

func (c *PropertiesConfigurationSource) SetLogger(l *log.Logger) {
	c.log = l
}

func (c *PropertiesConfigurationSource) Changes() <-chan ChangeSet {
	cs := make(chan ChangeSet)
	c.mutex.Lock()
	c.changes = append(c.changes, cs)
	c.mutex.Unlock()
	return cs
}

func (c *PropertiesConfigurationSource) Find(key string) (bool, interface{}) {
	// Register a watcher for it so that it gets automatically refreshed.
	c.mutex.Lock()
	defer c.mutex.Unlock()
	val, found := c.cache[key]
	return found, val
}

func (c *PropertiesConfigurationSource) Dispose() {
	c.mutex.RLock()
	chans := c.changes
	c.mutex.RUnlock()
	for _, ch := range chans {
		close(ch)
	}
}
