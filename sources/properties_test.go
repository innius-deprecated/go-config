package sources

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestPropertiesInterfaceCompliance(t *testing.T) {
	var s ConfigurationSource
	s = NewPropertiesSource("proptest.properties")
	assert.NotNil(t, s)
}

func TestDontPanicOnUnknownFile(t *testing.T) {
	s := NewPropertiesSource("idonotexist.properties")
	assert.NotNil(t, s)
}

func TestPropertiesFindKey(t *testing.T) {
	src := NewPropertiesSource("proptest.properties")
	found, fooval := src.Find("foo")
	found2, barval := src.Find("nested/bar")
	notfound, noneval := src.Find("abc")

	assert.True(t, found)
	assert.Equal(t, "fooval", fooval)

	assert.True(t, found2)
	assert.Equal(t, "barval", barval)

	assert.False(t, notfound)
	assert.Nil(t, noneval)
}

func TestPropertiesReadInteger(t *testing.T) {
	src := NewPropertiesSource("proptest.properties")
	found, ival := src.Find("integer")

	assert.True(t, found)
	assert.Equal(t, "5", ival)
}
