package config

import (
	"bitbucket.org/to-increase/go-config/types"
)

// Todo: optimize, for example with buckets per path chunk or something.
type store struct {
	data map[string]interface{}
}

func newStore() *store {
	return &store{
		data: make(map[string]interface{}),
	}
}

func (s *store) Has(p types.Path) bool {
	_, exists := s.data[p.GetPath()]
	return exists
}

func (s *store) Get(p types.Path) interface{} {
	if s.Has(p) {
		return s.data[p.GetPath()]
	} else {
		return nil
	}
}

func (s *store) Add(p types.Path, v interface{}) {
	s.data[p.GetPath()] = v
}
