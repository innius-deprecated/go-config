package config

import (
	"bitbucket.org/to-increase/go-config/types"
	"github.com/stretchr/testify/assert"
	"testing"
)

func path(path ...string) types.Path {
	return types.Path{
		Prefix: path[:len(path)-1],
		Key:    path[len(path)-1],
	}
}

func TestDoesNotExist(t *testing.T) {
	store := newStore()
	assert.False(t, store.Has(path("foo")))
	assert.False(t, store.Has(path("foo", "bar", "baz")))
}

func TestExists(t *testing.T) {
	store := newStore()
	p1 := path("foo")
	p2 := path("foo", "bar", "baz")
	store.Add(p1, "abc")
	store.Add(p2, "def")
	assert.True(t, store.Has(p1))
	assert.True(t, store.Has(p2))
}
