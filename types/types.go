package types

import (
	"strings"
)

const Separator = "/"

type DynamicValue interface {
	Update(Path, interface{})
}

type DynamicIntValue interface {
	Value() int
	OnChange(IntCallback)
	String() string
	SetDefaultOnChange(Callback)
}

type DynamicStringValue interface {
	Value() string
	OnChange(StringCallback)
	SetDefaultOnChange(Callback)
}

type DynamicBoolValue interface {
	Value() bool
	OnChange(BoolCallback)
	SetDefaultOnChange(Callback)
}

type IntCallback func(path Path, value int)
type StringCallback func(path Path, value string)
type BoolCallback func(path Path, value bool)

type Callback func(path Path, value interface{})

type Path struct {
	Prefix []string
	Key    string
}

func (p Path) GetPath() string {
	x := strings.Join(p.Prefix, Separator) + Separator + p.Key
	if strings.Index(x, Separator) == 0 {
		x = x[1:]
	}
	return x
}
