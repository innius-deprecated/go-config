package types

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestSimpleObject(t *testing.T) {
	p := &Path{
		Prefix: []string{"abc"},
		Key:    "def",
	}
	assert.Equal(t, "abc/def", p.GetPath())
	assert.Equal(t, 1, len(p.Prefix))
	assert.Equal(t, "abc", p.Prefix[0])
	assert.Equal(t, "def", p.Key)
}

func TestHierarchicalPaths(t *testing.T) {
	p := &Path{
		Prefix: []string{"abc", "def", "ghi"},
		Key:    "xyz",
	}
	assert.Equal(t, "abc/def/ghi/xyz", p.GetPath())
	assert.Equal(t, 3, len(p.Prefix))
	assert.Equal(t, "abc", p.Prefix[0])
	assert.Equal(t, "def", p.Prefix[1])
	assert.Equal(t, "ghi", p.Prefix[2])
	assert.Equal(t, "xyz", p.Key)
}
